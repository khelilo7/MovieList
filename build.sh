function build_and_release() {
    echo "Building..."
    service_name="movies_web"
    cd $service_name
    docker build -t khelilo73/listmovies:latest .
    docker image push khelilo73/listmovies:latest
    aws ecs update-service --cluster movies-cluster --service movies-service --force-new-deployment
}

function test() {

}