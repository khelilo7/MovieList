import requests
import json
from project.exception import ApiFailException
from requests.exceptions import HTTPError


def get_response_from_url(url, logger=None):
    r = requests.get(url)
    try:
        r.raise_for_status()
    except HTTPError as e:
        if logger:
            logger.error(e)
        raise ApiFailException
    return json.loads(r.content)