from project.utils import get_response_from_url
import project.server as server
from os import getenv


def get_people_movie_list():
    films_map = get_films_map()
    people = get_people()

    for person in people:
        for url in person.get("films", []):
            film_id = extract_id(url)
            if film_id in films_map:
                films_map[film_id]["people"].append(person["name"])
    return films_map.values()


def extract_id(url):
    if not url:
        return url
    return url.split("/")[-1]


def get_films_map():
    films = get_films()
    films_map = {}
    for film in films:
        film.update({"people": []})
        films_map.update({film["id"]: film})
    return films_map


def get_films():
    return get_response_from_url(getenv("BASE_URL") + "/films", server.app.logger)


def get_people():
    return get_response_from_url(getenv("BASE_URL") + "/people", server.app.logger)
