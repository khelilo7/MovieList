import os
import unittest
from project.movies import (
    extract_id,
    get_films_map,
    get_films,
    get_people,
    get_people_movie_list,
)
from project.server import app
from parameterized import parameterized
from mock import patch, Mock


class MoviesTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @parameterized.expand([("none url", None, None), ("ok url", "url/id", "id")])
    def test_extract_id(self, _, url, result):
        self.assertEqual(extract_id(url), result)

    @patch("project.movies.get_films")
    def test_get_film_map(self, p_films):
        p_films.return_value = []
        self.assertEqual(get_films_map(), {})

    @patch("project.movies.get_response_from_url")
    @patch("project.movies.getenv")
    def test_get_films(self, p_getenv, p_response):
        p_getenv.return_value = "BASE_URL"
        get_films()
        p_response.assert_called_once_with(p_getenv.return_value + "/films", app.logger)

    @patch("project.movies.get_response_from_url")
    @patch("project.movies.getenv")
    def test_get_people(self, p_getenv, p_response):
        p_getenv.return_value = "BASE_URL"
        get_people()
        p_response.assert_called_once_with(
            p_getenv.return_value + "/people", app.logger
        )

    @patch("project.movies.get_films_map")
    @patch("project.movies.get_people")
    def test_get_people_movie_list(self, p_people, p_films):
        p_people.return_value = [{"name": "person", "films": ["id"]}]
        p_films.return_value = {"id": {"people": []}}
        self.assertEqual(list(get_people_movie_list()), [{"people": ["person"]}])


if __name__ == "__main__":
    unittest.main()