import unittest
from mock import Mock, patch
from project.utils import get_response_from_url, ApiFailException
import requests
import json


class UtilsTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @patch("project.utils.requests")
    def test_response_from_url_raises(self, p_requests):
        response = Mock()
        response.raise_for_status.side_effect = requests.exceptions.HTTPError
        p_requests.get.return_value = response
        with self.assertRaises(ApiFailException):
            get_response_from_url("random_url")

    @patch("project.utils.requests")
    def test_response_from_url_success(self, p_requests):
        response = Mock()
        response.content = '["random response value"]'
        p_requests.get.return_value = response
        self.assertEquals(
            get_response_from_url("random_url"), json.loads(response.content)
        )
