import os
import unittest
from project.server import app, cache, make_response
from project.exception import ApiFailException
from mock import patch, Mock


class ServerTests(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        app.config["DEBUG"] = False

        with app.app_context():
            cache.clear()
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def tearDown(self):
        pass

    def test_healthy(self):
        response = self.app.get("/", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    @patch("project.server.app.logger.info")
    @patch("project.movies.get_people_movie_list")
    def test_api_fail(self, p_get_movies, p_logger):
        p_get_movies.side_effect = ApiFailException
        response = self.app.get("/movies", follow_redirects=True)
        p_logger.assert_called_once()
        self.assertEqual(response.status_code, 404)

    @patch("project.movies.get_people_movie_list")
    @patch("project.server.render_template")
    @patch("project.server.make_response")
    def test_api_sucess(self, p_make, p_render, p_get_movies):
        p_render.return_value = "random template"
        p_make.side_effect = make_response
        response = self.app.get("/movies", follow_redirects=True)
        p_make.assert_called_once_with("random template")
        p_render.assert_called_once_with("movies.html", data=p_get_movies.return_value)
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()