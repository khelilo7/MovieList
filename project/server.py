from flask import Flask, make_response, render_template, request, redirect
from flask_caching import Cache
from project.exception import ApiFailException
import requests
from project import movies

config = {
    "DEBUG": True,
    "CACHE_TYPE": "simple",
    "CACHE_DEFAULT_TIMEOUT": 300,
}


app = Flask(__name__)
app.config.from_mapping(config)
cache = Cache(app)


@app.route("/")
def health_check():
    app.logger.info("healthy")
    return "Up and running!!"


@app.route("/movies", methods=["GET"])
@cache.cached(timeout=60)
def list_movies():
    app.logger.info("getting movies")
    try:
        movies_list = movies.get_people_movie_list()
    except ApiFailException:
        return redirect(404)
    response = make_response(render_template("movies.html", data=movies_list))
    return response


@app.errorhandler(404)
def page_not_found(e):
    return {"length": None}, 404


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8000)
