# web application to list movies and people appering in these movies using flask,


## Start :

```shell
pip install -r requirements.txt
python server
```

## Start using docker : 

```shell
cd movies_web
 docker build -t movies .
 docker run -d -p 8000:8000 movies 
```

## Start tests 
```shell
python -m unittest discover .\project\tests\
```
